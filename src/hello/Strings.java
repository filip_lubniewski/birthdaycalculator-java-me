package hello;

/**
 *
 * @author macbookpro
 */
public interface Strings {
    String appName = "BirthdayCalculator";
    String welcomeMessage = "Hello user\n";
    String instruction = "Fill in you PESEL number if you want to know how many days are remaining to you birthday";
    String calculate = "Calculate";
    String goBack = "Back";


    String resultFormTitle = "Days unitl your birthday";
    String birthDateHeader = "date of your birth: ";
    String daysToBirthdayHeader = "days until birthday: ";

    String fristTwoDigitsOfBirthYear = "19";

    String sexHeader = "your sex: ";
    String male = "Male";
    String female = "Female"; 
}
