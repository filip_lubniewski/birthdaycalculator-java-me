package hello;

/**
 *
 * @author macbookpro
 */
public class SexUtils {
    public static String getSexFromPesel(String pesel) {
        int secondToLastDigit = Integer.valueOf(pesel.substring(9,10)).intValue();

        return secondToLastDigit % 2 == 0 ? Strings.female : Strings.male;
    }
}
