package hello;

import java.util.Calendar;
import java.util.Date;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author macbookpro
 */
public class BirthDateUtils {
    public static final int DAY_IN_SECONDS = 24 * 60 * 60;
    public static final int SECOND_IN_MILISECONDS = 1000;

    public static String convertPeselToBirthDate(String pesel) {

        return Strings.fristTwoDigitsOfBirthYear + pesel.substring(0,2) + "." + pesel.substring(2,4) + "." + pesel.substring(4,6);
    }

    public static int daysUntilBirthday(int birthMonth, int birthDay) {
        birthMonth = birthMonth - 1;

        Calendar today = Calendar.getInstance();
        
        Calendar birthday = Calendar.getInstance();
        birthday.set(Calendar.MONTH, birthMonth);
        birthday.set(Calendar.DAY_OF_MONTH, birthDay);

        if(today.after(birthday)) {
            birthday.set(Calendar.YEAR, today.get(Calendar.YEAR) + 1);
        }
        
        Date date1 = today.getTime();
        Date date2 = birthday.getTime();

        System.out.println(today.get(Calendar.DAY_OF_MONTH));
        System.out.println(today.get(Calendar.MONTH));
        System.out.println(today.get(Calendar.YEAR));

        int daysUntilBirthDay = (int) ((date2.getTime() - date1.getTime())/SECOND_IN_MILISECONDS/DAY_IN_SECONDS);

        return daysUntilBirthDay;
    }
}
