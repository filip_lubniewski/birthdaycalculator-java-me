package hello;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author macbookpro
 */
public class ResultScreen extends Form implements CommandListener {
    
     private Display display;
     private Command backCmd;

     private HelloMIDlet previousScreen;
    public ResultScreen(HelloMIDlet previousScreen){
        super(Strings.resultFormTitle);

        this.previousScreen = previousScreen;
        display = previousScreen.getDisplay();
        
        backCmd = new Command(Strings.goBack, Command.OK, 2);
        
        setupCurrentScreen();
    }

    private void setupCurrentScreen() {
        this.addCommand(backCmd);
        this.setCommandListener(this);
        showBirthdayInfo();
        showSexInfo();
    }
    
    private void showBirthdayInfo() {
      String dateOfBirth = BirthDateUtils.convertPeselToBirthDate(previousScreen.getPesel());
      
      showDate(dateOfBirth);
      showRemainingDaysToBirthday(dateOfBirth);
    }

    private void showDate(String dateOfBirth){
      this.append(Strings.birthDateHeader);
      this.append(dateOfBirth + "\n");
    }

    private void showRemainingDaysToBirthday(String dateOfBirth) {
      int birthMonth = Integer.valueOf(dateOfBirth.substring(5,7)).intValue();
      int birthDay = Integer.valueOf(dateOfBirth.substring(8,10)).intValue();

      this.append(Strings.daysToBirthdayHeader);
      this.append(String.valueOf(BirthDateUtils.daysUntilBirthday(birthMonth, birthDay)) + "\n");
    }

    private void showSexInfo(){
        this.append(Strings.sexHeader);
        this.append(SexUtils.getSexFromPesel(previousScreen.getPesel()));
    }

    public void commandAction(Command c, Displayable d) {
        String label = c.getLabel();

        if(label.equals(Strings.goBack)){
            display.setCurrent(previousScreen.getDisplayable());
        }
    }
}
