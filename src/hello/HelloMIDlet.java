package hello;

import java.util.Calendar;
import java.util.Date;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class HelloMIDlet extends MIDlet implements CommandListener{

  private static final int MAX_CHAR = 11;

  private static Display display;
  private Command calculate;
  private ResultScreen resultScreen;

  private Form form;
  private TextField pesel;

  public HelloMIDlet(){
    form = new Form(Strings.appName);
    pesel = new TextField(Strings.instruction, "", MAX_CHAR, TextField.NUMERIC);
    calculate = new Command(Strings.calculate, Command.OK, 2);
  }

  public void startApp(){
    display = Display.getDisplay(this);

    setupCurrentScreen();
  }

  private void setupCurrentScreen(){
    form.append(Strings.welcomeMessage);
    form.append(pesel);

    form.addCommand(calculate);
    form.setCommandListener(this);

    display.setCurrent(form);
  }

  public void pauseApp(){

  }

  public void destroyApp(boolean destroy){
    notifyDestroyed();
  }

  public void commandAction(Command c, Displayable d) {
    String label = c.getLabel();

    if(label.equals(Strings.calculate)){
        resultScreen = new ResultScreen(this);
        display.setCurrent(resultScreen);
    }
  }

  public Displayable getDisplayable() {
      return form;
  }

  public static Display getDisplay() {
    return display;
  }

  public Form getForm() {
      return form;
  }

  public String getPesel(){
    return pesel.getString();
  }
}
